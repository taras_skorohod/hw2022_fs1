package hw6.animals;

import hw6.interfeise.Pet;
import hw6.enumerations.Species;
import hw6.methods.Foul;

import java.util.Arrays;

public class Dog extends Pet implements Foul {

    Species species = Species.DOG;

    public Dog(String nickname, int age, byte trickLevel, String[]habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(species);
    }

    @Override
    protected void eat() {
        System.out.println("Я їм)");
    }

    @Override
    protected void respond() {
        System.out.printf("Привіт, хозяїн. Я %s. Я соскучився!\n", this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }


}
