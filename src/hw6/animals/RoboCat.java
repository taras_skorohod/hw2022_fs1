package hw6.animals;

import hw6.interfeise.Pet;
import hw6.enumerations.Species;
import hw6.methods.Foul;

import java.util.Arrays;

public class RoboCat extends Pet implements Foul {
    Species species = Species.ROBOCAT;


    public RoboCat(String nickname, int age, byte trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(species);
    }
    @Override
    protected void eat() {
        System.out.println("Я не їм)");
    }

    @Override
    public void respond() {
        System.out.println("Привіт хозяїн!");
    }

    @Override
    public void foul() {
        System.out.println("Я робот я не можу наслідити)");
    }


}
