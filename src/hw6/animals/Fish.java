package hw6.animals;

import hw6.interfeise.Pet;
import hw6.enumerations.Species;
import hw6.methods.Foul;

import java.util.Arrays;

public class Fish extends Pet implements Foul {

    Species species = Species.FISH;

    public Fish (String nickname, int age, byte trickLevel, String[]habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(species);
    }
    @Override
    protected void eat() {
        System.out.println("Я їм)");
    }
    @Override
    public void respond() {
        System.out.printf("Я не вмію розмовляти. Я рибка %s\n",this.getNickname());
    }


    @Override
    public void foul() {
        System.out.println("Буль...");
    }

}
