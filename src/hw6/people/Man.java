package hw6.people;

import hw6.Family;
import hw6.interfeise.Human;

final public class Man extends Human {
    public Man(String name, String surname, int year, byte iq, String[][] schedule, Family family){
        super(name, surname, year, iq, schedule, family);
    }

    @Override
    public void describePet() {
        System.out.printf("У мене є %s, йому %s років, він %s\n",
                this.getFamily().getPet().getSpecies(), this.getFamily().getPet().getAge(), (this.getFamily().getPet().getTrickLevel() > 50 ? "дуже хитрий" : "не дуже хитрий"));
    }
    @Override
    public void repairCar(){
        System.out.println("Ремонтувати машину...");
    }

}
