package hw9;

import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;
import java.util.HashMap;

abstract class Human {
    enum DayOfWeek {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }
    String name;
    String surname;
    long birthDate = System.currentTimeMillis();
    int year;
    int iq;
    Pet myPet;
    static Human mother;
    static Human father;
    int day;
    int month;

    public HashMap<String, String> hashMapSchedule() {
        HashMap<String, String> scheduleMap = new HashMap<>();

        scheduleMap.put(Human.DayOfWeek.MONDAY.name(), "Іти до школи");
        scheduleMap.put(Human.DayOfWeek.TUESDAY.name(), "Іди в магазин");
        scheduleMap.put(Human.DayOfWeek.WEDNESDAY.name(), "Вирушайте подорожувати");
        scheduleMap.put(Human.DayOfWeek.THURSDAY.name(), "Домашня робота");
        scheduleMap.put(Human.DayOfWeek.FRIDAY.name(), "Прогулянка");
        scheduleMap.put(Human.DayOfWeek.SATURDAY.name(), "Спати");
        scheduleMap.put(Human.DayOfWeek.SUNDAY.name(), "Спати");

        return scheduleMap;
    }


    Family family;

    public void createFamily(Human mother, Human father, Human[] childrens) {
        family.setFamily(mother, father, childrens);
    }

    public abstract Human showFather();
    public abstract Human showMother();
    public abstract void greetPet();
    public abstract void describePet();
    public abstract void makeUp();
    public abstract void repairCar();
    public abstract void describeAge();

    public static void havePet(String s, int i, int i2) {
        System.out.println("В меня є " + s + ",");
        System.out.println("йому " + i + "рік(о),");
        if (i2 > 50) {
            System.out.println("він дуже хитрий");
        } else if (i2 < 50) {
            System.out.println("він почті не хитрий");
        }
    }

    @Override
    public String toString() {
        String infoHuman = "Імя: " + name + ", Фамілія: " + surname + ", рік: " + year + ", графік: " + hashMapSchedule();
        System.out.println(infoHuman);
        return infoHuman;
    }

    public static void getAgeHuman(long birthDate, int year, int month, int day) {
        SimpleDateFormat YearFormat = new SimpleDateFormat("yyyy");
        SimpleDateFormat MonthFormat = new SimpleDateFormat("MM");
        SimpleDateFormat DayFormat = new SimpleDateFormat("dd");

        Date resultDateFormat = new Date(birthDate);

        String showYear = YearFormat.format(resultDateFormat);
        String showMonth = MonthFormat.format(resultDateFormat);
        String showDay = DayFormat.format(resultDateFormat);

        LocalDate oldDate = LocalDate.of(year, month, day);
        LocalDate newDate = LocalDate.of(Integer.parseInt(showYear), Integer.parseInt(showMonth), Integer.parseInt(showDay));

        Period period = Period.between(oldDate, newDate);

        System.out.print(period.getYears() + " рік,");
        System.out.print(period.getMonths() + " місяць,");
        System.out.print(period.getDays() + " день");
    }


}

class Man extends Human {
    Man() {
        super();
        name = "";
        surname = "";
        year = 0;
        iq = -1;
        myPet = null;
        mother = null;
        father = null;
        family = null;
    }

    public Man(String name, String surname, int year) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Man(String name, String surname, int year, int iq, Pet pet, Human mother, Human father) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.myPet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Man(String name, String surname, int day, int month, int year) {
        super();
        this.name = name;
        this.surname = surname;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public Human showFather() {
        return father;
    }
    public Human showMother() {
        return mother;
    }
    public void repairCar() {
        System.out.println("Пора чинити машину!");
    }

    public void describeAge() {
        getAgeHuman(birthDate, year, month, day);
    }

    public void greetPet() {
        System.out.printf("Привіт - %s, це я твій хозяїн - %s", myPet.namePet(), name);
    }
    public void describePet() {
        havePet(myPet.SpeciesPet(), myPet.AgePet(), myPet.trickLevelPet());
    }
    public void makeUp() {}
}
class Woman extends Human {


    Woman() {
        super();
        name = "";
        surname = "";
        year = 0;
        iq = -1;
        myPet = null;
        mother = null;
        father = null;
        family = null;
        day = 0;
        month = 0;
    }

    public Woman(String name, String surname, int year) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Woman(String name, String surname, int year, int iq, Pet pet, Human mother, Human father) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.myPet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Woman(String name, String surname, int day, int month, int year) {
        super();
        this.name = name;
        this.surname = surname;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public Human showFather() {
        return father;
    }
    public Human showMother() {
        return mother;
    }
    public void makeUp() {
        System.out.println("Траеба пофабуватися");
    }
    public void repairCar() {}

    public void describeAge() {
        getAgeHuman(birthDate, year, month, day);
    }

    public void greetPet() {
        System.out.printf("Привіт - %s, це я твоя хозяйка - %s", myPet.namePet(), name);
    }
    public void describePet() {
        havePet(myPet.SpeciesPet(), myPet.AgePet(), myPet.trickLevelPet());
    }
}



class Children extends Human {
    Children() {
        super();
        name = "";
        surname = "";
        birthDate = 0;
        iq = -1;
        myPet = null;
        mother = null;
        father = null;
        family = null;
    }

    public Children(String name, String surname, int year) {
        super();
        this.name = name;
        this.surname = surname;
        this.birthDate = year;
    }

    public Children(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        super();
        this.name = name;
        this.surname = surname;
        this.birthDate = year;
        this.iq = iq;
        this.myPet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Children(String name, String surname, int day, int month, int year, int iq) {
        super();
        this.name = name;
        this.surname = surname;
        this.day = day;
        this.month = month;
        this.year = year;
        this.iq = iq;
    }

    public Human showFather() {
        return father;
    }
    public Human showMother() {
        return mother;
    }
    public void makeUp() {

    }
    public void repairCar() {

    }

    public void describeAge() {
        getAgeHuman(birthDate, year, month, day);
    }

    @Override
    public String toString() {
        String dobChildren = day + "/" + month + "/" + year;
        System.out.println(dobChildren);
        return dobChildren;
    }

    public void greetPet() {
        System.out.printf("Привіт - %s, це я твоя хозяйка - %s", myPet.namePet(), name);
    }
    public void describePet() {
        havePet(myPet.SpeciesPet(), myPet.AgePet(), myPet.trickLevelPet());
    }
}