package hw7.animals;

import hw7.enumerations.Species;
import hw7.interfeise.Pet;
import hw7.methods.Foul;

import java.util.Set;

public class RoboCat extends Pet implements Foul {
    Species species = Species.ROBOCAT;


    public RoboCat(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(species);
    }
    @Override
    protected void eat() {
        System.out.println("Я не їм)");
    }

    @Override
    public void respond() {
        System.out.println("Привіт хозяїн!");
    }

    @Override
    public void foul() {
        System.out.println("Я робот я не можу наслідити)");
    }


}
