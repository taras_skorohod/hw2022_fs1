package hw7.animals;

import hw7.enumerations.Species;
import hw7.interfeise.Pet;
import hw7.methods.Foul;

import java.util.Set;

public class Fish extends Pet implements Foul {

    Species species = Species.FISH;

    public Fish (String nickname, int age, byte trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(species);
    }
    @Override
    protected void eat() {
        System.out.println("Я їм)");
    }
    @Override
    public void respond() {
        System.out.printf("Я не вмію розмовляти. Я рибка %s\n",this.getNickname());
    }


    @Override
    public void foul() {
        System.out.println("Буль...");
    }

}
