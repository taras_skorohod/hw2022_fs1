package hw7.animals;

import hw7.enumerations.Species;
import hw7.interfeise.Pet;
import hw7.methods.Foul;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {
    Species species = Species.DOMESTICCAT;
    public DomesticCat(String nickname, int age, byte trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(species);
    }
    @Override
    protected void eat() {
        System.out.println("Я їм)");
    }
    @Override
    public void respond() {
        System.out.printf("Привіт, хозяїн. Я кіт %s.!\n", this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }



}
