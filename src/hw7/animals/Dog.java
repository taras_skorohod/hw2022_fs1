package hw7.animals;

import hw7.enumerations.Species;
import hw7.interfeise.Pet;
import hw7.methods.Foul;

import java.util.Set;

public class Dog extends Pet implements Foul {

    Species species = Species.DOG;

    public Dog(String nickname, int age, byte trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
        this.setSpecies(species);
    }

    @Override
    protected void eat() {
        System.out.println("Я їм)");
    }

    @Override
    protected void respond() {
        System.out.printf("Привіт, хозяїн. Я %s. Я соскучився!\n", this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }


}
