package hw7.people;

import hw7.Family;
import hw7.interfeise.Human;

import java.util.Map;

final public class children extends Human {
    public children(String name, String surname, int year){
        super(name, surname, year);
    }
    @Override
    protected void describePet() {
        System.out.printf("У мене є %s, йому %s років, він %s\n",
                this.getFamily().getPet().getSpecies(), this.getFamily().getPet().getAge(), (this.getFamily().getPet().getTrickLevel() > 50 ? "дуже хитрий" : "не дуже хитрий"));
    }

    @Override
    protected void repairCar() {

        System.out.println("Привіт");
    }
}
