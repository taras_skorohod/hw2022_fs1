package hw7.enumerations;

public enum Species {
    CAT,
    DOG,
    PARROT,
    DOMESTICCAT,
    FISH,
    ROBOCAT,
    UNKNOWN

}
