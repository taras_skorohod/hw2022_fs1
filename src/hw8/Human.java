package hw8;

import java.util.HashMap;

abstract class Human {
    enum DayOfWeek {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }
    String name;
    String surname;
    int year;
    int iq;
    Pet myPet;
    static Human mother;
    static Human father;

    public HashMap<String, String> hashMapSchedule() {
        HashMap<String, String> scheduleMap = new HashMap<>();

        scheduleMap.put(DayOfWeek.MONDAY.name(), "Іти до школи");
        scheduleMap.put(DayOfWeek.TUESDAY.name(), "Іди в магазин");
        scheduleMap.put(DayOfWeek.WEDNESDAY.name(), "Вирушайте подорожувати");
        scheduleMap.put(DayOfWeek.THURSDAY.name(), "Домашня робота");
        scheduleMap.put(DayOfWeek.FRIDAY.name(), "Прогулянка");
        scheduleMap.put(DayOfWeek.SATURDAY.name(), "Спати");
        scheduleMap.put(DayOfWeek.SUNDAY.name(), "Спати");

        return scheduleMap;
    }


    Family family;

    public void createFamily(Human mother, Human father, Human[] childrens) {
        family.setFamily(mother, father, childrens);
    }

    public abstract Human showFather();
    public abstract Human showMother();
    public abstract void greetPet();
    public abstract void describePet();
    public abstract void makeUp();
    public abstract void repairCar();

    public static void havePet(String s, int i, int i2) {
        System.out.println("В меня є " + s + ",");
        System.out.println("йому " + i + "рік(о),");
        if (i2 > 50) {
            System.out.println("він дуже хитрий");
        } else if (i2 < 50) {
            System.out.println("він почті не хитрий");
        }
    }

    @Override
    public String toString() {
        String infoHuman = "Імя: " + name + ", Фамілія: " + surname + ", рік: " + year + ", графік: " + hashMapSchedule();
        System.out.println(infoHuman);
        return infoHuman;
    }
}

class Man extends Human {
    Man() {
        super();
        name = "";
        surname = "";
        year = -1;
        iq = -1;
        myPet = null;
        mother = null;
        father = null;
        family = null;
    }

    public Man(String name, String surname, int year) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Man(String name, String surname, int year, int iq, Pet pet, Human mother, Human father) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.myPet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Human showFather() {
        return father;
    }
    public Human showMother() {
        return mother;
    }
    public void repairCar() {
        System.out.println("Пора чинити машину!");
    }
    public void greetPet() {
        System.out.printf("Привіт - %s, це я твій хозяїн - %s", myPet.namePet(), name);
    }
    public void describePet() {
        havePet(myPet.SpeciesPet(), myPet.AgePet(), myPet.trickLevelPet());
    }
    public void makeUp() {}
}
class Woman extends Human {
    Woman() {
        super();
        name = "";
        surname = "";
        year = -1;
        iq = -1;
        myPet = null;
        mother = null;
        father = null;
        family = null;
    }

    public Woman(String name, String surname, int year) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Woman(String name, String surname, int year, int iq, Pet pet, Human mother, Human father) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.myPet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Human showFather() {
        return father;
    }
    public Human showMother() {
        return mother;
    }
    public void makeUp() {
        System.out.println("Треба підкрипитися");
    }
    public void repairCar() {}
    public void greetPet() {
        System.out.printf("Привіт - %s, це я твоя хозяйка - %s", myPet.namePet(), name);
    }
    public void describePet() {
        havePet(myPet.SpeciesPet(), myPet.AgePet(), myPet.trickLevelPet());
    }
}
class Children extends Human {
    Children() {
        super();
        name = "";
        surname = "";
        year = -1;
        iq = -1;
        myPet = null;
        mother = null;
        father = null;
        family = null;
    }

    public Children(String name, String surname, int year) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Children(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        super();
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.myPet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Human showFather() {
        return father;
    }
    public Human showMother() {
        return mother;
    }
    public void makeUp() {

    }
    public void repairCar() {

    }
    public void greetPet() {
        System.out.printf("Привіт - %s, це я твоя хозяйка - %s", myPet.namePet(), name);
    }
    public void describePet() {
        havePet(myPet.SpeciesPet(), myPet.AgePet(), myPet.trickLevelPet());
    }
}