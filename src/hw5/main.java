package hw5;

import hw4.Family;
import hw4.Pet;

import java.util.ArrayList;
import java.util.List;

public class main {

    public static void main(String[] args) {

        String[][] schedule = new String[1][2];

        schedule[0][0] = DayOfWeek.FRIDAY.name();
        schedule[0][1] = "write code";

        Human h1 = new Human();
        h1.setSchedule(schedule);

        System.out.println(h1);

    }
}
