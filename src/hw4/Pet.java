package hw4;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private String nickname;
    private int age;
    private int trickLevel;
    private String [] habits;
    private String species;

    public Pet(String nickname, int age, int trickLevel, String[] habits, String species) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.species = species;
    }

    @Override
    public String toString() {
        String classInfo = String.format("%s{" +
                        " nickname='%s'" +
                        ", age=%s" +
                        ", trickLevel=%s" +
                        ", habits=%s" +
                        '}',
                species, nickname, age, trickLevel, Arrays.toString(habits));

        return classInfo;
    }


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && trickLevel == pet.trickLevel && Objects.equals(nickname, pet.nickname) && Arrays.equals(habits, pet.habits) && Objects.equals(species, pet.species);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(nickname, age, trickLevel, species);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public static void eat (){
        System.out.println("Я їм!");
    }
    public void respond(){
        System.out.printf("Привіт, хазяїне. Я – %s. Я скучив! \n", this.nickname);
    }
    public static void foul(){
        System.out.println("Потрібно добре замісти сліди...");
    }
}
