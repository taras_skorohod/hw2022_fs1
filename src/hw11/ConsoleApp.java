package hw11;

import java.text.SimpleDateFormat;
import java.util.*;

public class ConsoleApp {
  public static Human father;
  public static Human mother;
  public static Family family;
  static List<Family> arrFamily = new ArrayList<Family>();

  public static Human childrenTemplate;

  public static void fatherObj(Scanner sc) {
    System.out.print("Enter father: (example: name, surname, dayOfBirthday, monthOfBirthday, yearOfBirthday, IQ). \n");
    System.out.print("Name => ");
    String name = sc.next();
    System.out.print("Surname => ");
    String surname = sc.next();
    System.out.print("DayOfBirthday => ");
    Integer dayOfBirthday = sc.nextInt();
    System.out.print("MonthOfBirthday => ");
    Integer monthOfBirthday = sc.nextInt();
    System.out.print("YearOfBirthday => ");
    Integer yearOfBirthday = sc.nextInt();
    System.out.print("IQ => ");
    Integer IQ = sc.nextInt();

    father = new Man(
      name, surname,
      dayOfBirthday,
      monthOfBirthday,
      yearOfBirthday,
      IQ
    );

  }
  public static void motherObj(Scanner sc) {
    System.out.print("Enter mother: (example: name, surname, dayOfBirthday, monthOfBirthday, yearOfBirthday, IQ). \n");
    System.out.print("Name => ");
    String name = sc.next();
    System.out.print("Surname => ");
    String surname = sc.next();
    System.out.print("DayOfBirthday => ");
    Integer dayOfBirthday = sc.nextInt();
    System.out.print("MonthOfBirthday => ");
    Integer monthOfBirthday = sc.nextInt();
    System.out.print("YearOfBirthday => ");
    Integer yearOfBirthday = sc.nextInt();
    System.out.print("IQ => ");
    Integer IQ = sc.nextInt();

    mother = new Woman(
      name,
      surname,
      dayOfBirthday,
      monthOfBirthday,
      yearOfBirthday,
      IQ
    );
  }

  public static void sendError(String message) {
    System.out.println("Error: " + message);
  }

  public static void sendEditFamily() {
    System.out.println("1. Народити дитину");
    System.out.println("2. Повернутися до головного меню");
    System.out.println("Вибір команди: ");
    System.out.print("=>");
  }

  public static void startApp() {
    Scanner scan = new Scanner(System.in);
    String cmdNumber;

    do {
      HelpText.text();
      System.out.print("=> ");
      cmdNumber = scan.next();
      switch (cmdNumber) {
        case "1":
          System.out.println("Відбувається наповнення даними");

          for (int i = 0; i < RandomInteger.getRandomNumberForFamily(); i++) {
            father = new Man("Father #" + i, "Surname", 12, 12, 1980, 100);
            mother = new Woman("Mother #" + i, "Surname", 21, 11, 1982, 100);
            childrenTemplate = new Children("Children #" + i, "Surname", 13, 10, 2013, 100, mother, father, "boy", "null", "null");
            family = new Family(mother, father);

            for (int j = 0; j < RandomInteger.getRandomNumberForChildren(); j++) {
              family.addChild(childrenTemplate);
            }

            arrFamily.add(family);
          }

          System.out.println("Наповнення даними завершено");
          break;
        case "2":
          if (arrFamily.isEmpty()) {
            sendError("Масив сімей порожній!");
          } else {
            System.out.println();
            System.out.println("=============");
            for (int i = 0; i < arrFamily.size(); i++) {
              Family fakeFamily = arrFamily.get(i);
              fakeFamily.prettyFormat(i);
            }
          }
          break;
        case "3":
          if (arrFamily.isEmpty()) {
            sendError("Масив сімей порожній!");
          } else {
            System.out.print("Відобразити сім'ї, де кількість людей більша за задану, введіть число: ");
            int countBigFamily = scan.nextInt();
            System.out.println();

            for (int i = 0; i < arrFamily.size(); i++) {
              Family countFamily = arrFamily.get(i);

              if (countFamily.childrens.size() + 2 > countBigFamily) {
                countFamily.prettyFormat(i);
              }
            }
          }
          break;
        case "4":
          if (arrFamily.isEmpty()) {
            sendError("Масив сімей порожній!");
          } else {
            System.out.print("Відобразити сім'ї, де кількість людей менша за задану, введіть число: ");
            int countBigFamily = scan.nextInt();
            System.out.println();

            for (int i = 0; i < arrFamily.size(); i++) {
              Family countFamily = arrFamily.get(i);

              if (countFamily.childrens.size() + 2 < countBigFamily) {
                countFamily.prettyFormat(i);
              }
            }
          }
          break;
        case "5":
          if (arrFamily.isEmpty()) {
            sendError("Масив сімей порожній!");
          } else {
            System.out.print("Відобразити сім'ї, де кількість людей дорівнює заданому, введіть число: ");
            int qeualsFamily = scan.nextInt();
            System.out.println();

            for (int i = 0; i < arrFamily.size(); i++) {
              Family countFamily = arrFamily.get(i);

              if (countFamily.childrens.size() + 2 == qeualsFamily) {
                countFamily.prettyFormat(i);
              }
            }
          }
          break;
        case "6":
          System.out.println("Створення нової родини\n");

          System.out.println("Створення об'єкту мама");
          motherObj(scan);

          System.out.println("\nСтворення об'єкта батько");
          fatherObj(scan);

          family = new Family(mother, father);
          arrFamily.add(family);
          break;
        case "7":
          System.out.println("Вилучення сім'ї за індексом (ID).");

          for (int i = 0; i < arrFamily.size(); i++) {
            System.out.println("Family index - " + i);
          }

          System.out.print("Виберіть доступний індекс: ");
          int indexFamily = scan.nextInt();

          for (int i = 0; i < arrFamily.size(); i++) {
            if (indexFamily == i) {
              arrFamily.remove(indexFamily);
            }
          }

          System.out.println("Сім'я була успішно вилучена!");
        case "8":
          System.out.println("Редагування сім'ї за індексом (ID).");

          for (int i = 0; i < arrFamily.size(); i++) {
            System.out.println("Family index - " + i);
          }

          System.out.print("Виберіть доступний індекс: ");
          int editIndexFamily = scan.nextInt();

          for (int i = 0; i < arrFamily.size(); i++) {
            Family thisFamily = arrFamily.get(i);

            if (editIndexFamily == i) {
              thisFamily.prettyFormat(i);
              System.out.println();

              sendEditFamily();
              int editFamily = scan.nextInt();

              switch (editFamily) {
                case 1:
                  System.out.print("Введіть кількість дітей: ");
                  int countChild = scan.nextInt();

                  for (int j = 0; j < countChild; j++) {
                    thisFamily.addChild(childrenTemplate);
                  }
                  break;
                case 2:
                  break;
              }
            }
          }
        case "9":
          System.out.print("Введіть вік, після якого видаляться всі діти із сімей: ");
          int ageChild = scan.nextInt();

          Date date = new Date();
          SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
          String dateNow = formatter.format(date);

          for (int i = 0; i < arrFamily.size(); i++) {
            Family delChildFromFamily = arrFamily.get(i);
            for (int j = 0; j < delChildFromFamily.childrens.size(); j++) {
              Human item = arrFamily.get(i).childrens.get(j);

              System.out.println(Integer.parseInt(dateNow) - item.year);
              if (Integer.parseInt(dateNow) - item.year > ageChild) {
                for (int k = 0; k < delChildFromFamily.childrens.size(); k++) {
                  delChildFromFamily.deleteChild(k);
                  delChildFromFamily.prettyFormat(j);
                }
              }
            }
          }

        }
    }
    while (!Objects.equals(cmdNumber, "exit")) ;
    System.out.println("Програма успішно завершена)");
  }
}
